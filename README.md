# PhpStorm.cmd
Add `Open with PhpStorm` to Windows right click context menu

---------------------------------------------------------------------------

با استفاده از این قابلیت میتوانید به راست کلیک ویندوز باز کردن با پی اچ پی استورم را اضافه نمایید


### Follow me on: 
* blog : [alirezap30web.ir](https://alirezap30web.ir/)
#####
* instagram : [instagram.com/alirezap30web/](https://www.instagram.com/alirezap30web/)
#####
* twitter :  [twitter.com/alirezap30web](https://twitter.com/alirezap30web)
#####
* linkedin : [linkedin.com/in/alirezap30web/](https://www.linkedin.com/in/alirezap30web/)
######
* virgool : [virgool.io/@alirezap30web](https://virgool.io/@alirezap30web)